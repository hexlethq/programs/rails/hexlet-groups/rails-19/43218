# frozen_string_literal: true

def make_censored(text, stop_words)
  # BEGIN
  return if stop_words.empty? || text.empty?

  text
    .split
    .map { |word| stop_words.include?(word) ? '$#%!' : word }
    .join(' ')
  # END
end
