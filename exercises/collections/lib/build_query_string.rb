# frozen_string_literal: true

# BEGIN
def build_query_string(data)
  data
    .sort
    .map { |(key, value)| "#{key}=#{value}" } .join('&')
end
# END
