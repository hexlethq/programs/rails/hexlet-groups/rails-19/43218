# frozen_string_literal: true

# BEGIN
def compare_versions(ver1, ver2)
  return 0 if ver1 == ver2

  transposed = [ver1, ver2].map { |version| version.split('.').map(&:to_i) }.transpose
  (major1, major2), (minor1, minor2) = transposed
  major1 == major2 ? minor1 <=> minor2 : major1 <=> major2
end
# END
