# frozen_string_literal: true

# BEGIN
def get_same_parity(list)
  return list if list.length <= 1

  head_elem_parity = list[0].even? ? :even? : :odd?
  list.filter(&head_elem_parity)
end
# END
