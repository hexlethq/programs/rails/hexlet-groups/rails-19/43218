# frozen_string_literal: true

def count_by_years(users)
  users
    .filter { |user| user[:gender] == 'male' }
    .map { |user| Time.new(user[:birthday]).year.to_s }
    .each_with_object({}) do |year, acc|
      count = acc.fetch(year, 0)
      acc[year] = count + 1
      acc
    end
end
