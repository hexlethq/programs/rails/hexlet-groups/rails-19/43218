# frozen_string_literal: true

# BEGIN
def anagramm_filter(origin_word, check_list)
  check_list.filter(&lambda { |word|
    return true if word == origin_word
    return false if word.length != origin_word.length

    word.chars.all? { |char| origin_word.count(char) == word.count(char) }
  })
end
# END
