# frozen_string_literal: true

# BEGIN
module Model
  attr_accessor :attributes, :scheme

  def self.included(base)
    base.extend(ExtendedModule)
  end

  def initialize(attrs = {})
    scheme = (self.class.instance_variable_get :@scheme) || {}
    attributes = {}

    scheme.each do |name, attr_data|
      type = attr_data[:type]
      default_value = attrs.fetch(name, attr_data[:default])
      converted_value = self.class.convert(default_value, type)
      attributes[name] = converted_value
    end

    @scheme = scheme
    @attributes = attributes
  end

  module ExtendedModule
    TYPES_CONVERTERS = {
      string: ->(a) { a.to_s },
      datetime: ->(a) { a.nil? ? a : DateTime.parse(a) },
      boolean: ->(a) { !!a },
      integer: ->(a) { a.to_i }
    }.freeze

    def convert(value, type)
      return nil if value.nil?

      converter = TYPES_CONVERTERS.fetch(type, ->(x) { x })
      converter.call(value)
    end

    def define_attr_setter(name)
      define_method("#{name}=") do |value|
        type = scheme[name][:type]
        converted_value = self.class.convert(value, type)
        attributes[name] = converted_value
      end
    end

    def define_attr_getter(name)
      define_method(name) do
        attributes[name]
      end
    end

    def attribute(name, **args)
      default_value = args[:default]
      type = args[:type]
      scheme = instance_variable_get(:@scheme) || {}
      scheme[name] = {
        default: default_value,
        type: type
      }
      instance_variable_set(:@scheme, scheme)
      define_attr_setter(name)
      define_attr_getter(name)
    end
  end
end
# END
