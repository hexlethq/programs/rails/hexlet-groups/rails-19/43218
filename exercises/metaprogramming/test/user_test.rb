# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/user'

class UserTest < Minitest::Test
  # BEGIN
  def test_user
    result_attributes = { name: 'Andrey', birthday: nil, active: false }
    user = User.new
    user.active = true
    assert_equal result_attributes.merge({ active: true }), user.attributes
  end
  # END
end
