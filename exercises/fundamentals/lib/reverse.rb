# frozen_string_literal: true

# BEGIN
def reverse(str)
  return str if str.length <= 1

  i = 0
  reversed_str = ''
  while i != str.length
    reversed_str = str[i] + reversed_str
    i += 1
  end
  reversed_str
end
# END
