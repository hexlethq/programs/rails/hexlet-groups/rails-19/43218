# frozen_string_literal: true

# BEGIN
def fibonacci(num)
  if num <= 0
    nil
  elsif [1, 2].include? num
    num - 1
  else
    fibonacci(num - 1) + fibonacci(num - 2)
  end
end
# END
