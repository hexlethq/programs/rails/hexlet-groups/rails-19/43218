# frozen_string_literal: true

# BEGIN
def get_fizz_buzz_word(num)
  if (num % 3).zero? && (num % 5).zero?
    'FizzBuzz'
  elsif (num % 3).zero?
    'Fizz'
  elsif (num % 5).zero?
    'Buzz'
  else
    num
  end
end

def fizz_buzz(start, stop)
  acc = ''
  return acc if start > stop

  count = start
  while count <= stop
    separator = acc.empty? ? '' : ' '
    acc += "#{separator}#{get_fizz_buzz_word count}"
    count += 1
  end
  acc
end
# END
