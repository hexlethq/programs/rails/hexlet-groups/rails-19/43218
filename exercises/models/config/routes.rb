# frozen_string_literal: true

Rails.application.routes.draw do
  # BEGIN
  root "articles#index"
  resources "articles", only: "show"
  # END
end
