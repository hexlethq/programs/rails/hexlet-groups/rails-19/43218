# frozen_string_literal: true

require 'test_helper'

class ArticlesTest < ActionDispatch::IntegrationTest
  setup do
    @article = articles(:one)
  end

  test 'should get index' do
    get '/'
    assert_match 'Articles', @response.body
    assert_response :success
  end

  test 'should show article' do
    get article_url @article
    assert_match @article.title, @response.body
    assert_match @article.body, @response.body
    assert_response :success
  end
end
