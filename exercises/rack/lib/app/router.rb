# frozen_string_literal: true

class Router
  def call(env)
    body = ''
    code = 200
    case env['PATH_INFO']
    when '/'
      body = 'Hello, World!'
    when '/about'
      body = 'About page'
    else
      body = '404 Not Found'
      code = 404
    end
    [code, { 'Content-Type' => 'text/plain' }, [body]]
  end
end
