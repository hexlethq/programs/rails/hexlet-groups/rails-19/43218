# frozen_string_literal: true

require 'digest'

class Signature
  def initialize(app)
    @app = app
  end

  def call(env)
    code, headers, body = @app.call(env)
    encoded_body = Digest::SHA2.hexdigest body.last
    [code, headers, body << encoded_body]
  end
end
