# frozen_string_literal: true

# BEGIN
require 'uri'
require 'forwardable'

class Url
  extend Forwardable
  include Comparable

  attr_reader :url

  def_delegators :url, :host, :scheme

  def initialize(url)
    @url = URI(url)
  end

  def <=>(other)
    url <=> other.url
  end

  def query_param(key1, default = nil)
    return unless query_params

    query_params.fetch(key1, default)
  end

  def query_params
    return unless url.query

    url
      .query
      .split('&')
      .each_with_object({}) do |item, acc|
        key, value = item.split('=')
        acc[key.to_sym] = value
        acc
      end
  end
end
# END
