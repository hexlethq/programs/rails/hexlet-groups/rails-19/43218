# frozen_string_literal: true

require_relative 'test_helper'
require_relative '../lib/stack'

class StackTest < Minitest::Test
  # BEGIN
  def setup
    @stack = Stack.new
  end

  def test_is_stack_empty
    assert_empty @stack
  end

  def test_push_pop
    arr = Array.new(rand(1..10)) { rand(1..10) }
    arr.each { |el| @stack.push!(el) }
    assert_equal(arr, @stack.to_a)

    prev_top = @stack.pop!
    assert_equal(arr.last, prev_top)
  end

  def test_clear
    arr = Array.new(rand(1..10)) { rand(1..10) }
    arr.each { |el| @stack.push!(el) }
    @stack.clear!
    assert_empty @stack
  end
  # END
end

test_methods = StackTest.new({}).methods.select { |method| method.start_with? 'test_' }
raise 'StackTest has not tests!' if test_methods.empty?
